# TRS-MRI-Models

These models were created by Tarun Eswar, a high school student at the Massachusetts Academy of Math & Science. They were made publically available from the following <a href="https://aps.arxiv.org/abs/2306.12435">paper</a>. Additional or partially developed models not mentioned in the paper are available in this repository. All information can be found in <a href="https://gitlab03.wpi.edu/teswar/trs-mri-models/-/blob/main/Model-Info.tsv">Model-Info.tsv</a>.

**Abstract** Obsessive-compulsive disorder (OCD) presents itself as a highly debilitating disorder. The disorder has common associations with the prefrontal cortex and the glutamate receptor known as Metabotropic Glutamate Receptor 5 (mGluR5). This receptor has been observed to demonstrate higher levels of signaling from positron emission tomography scans measured by its distribution volume ratios in mice. Despite this evidence, studies are unable to fully verify the involvement of mGluR5 as more empirical data is needed. Computational modeling methods were used as a means of validation for previous hypotheses involving mGluR5. The inadequacies in relation to the causal factor of OCD were answered by utilizing T1 resting-state magnetic resonance imaging (TRS-MRI) scans of patients suffering from schizophrenia, major depressive disorder, and obsessive-compulsive disorder. Because comorbid cases often occur within these disorders, cross-comparative abilities become necessary to find distinctive characteristics. Two-dimensional convolutional neural networks alongside ResNet50 and MobileNet models were constructed and evaluated for efficiency. Activation heatmaps of TRS-
MRI scans were outputted, allowing for transcriptomics analysis. Though, a lack of ability to predict OCD cases prevented gene expression analysis. Across all models, there was an 88.75% validation accuracy for MDD, and 82.08% validation accuracy for SZD under the framework of ResNet50 as well as novel computation. OCD yielded an accuracy rate of 54.4%. These results provided further evidence for the p-factor theory regarding mental disorders. Future work involves the application of alternate transfer learning networks than those used in this paper to bolster accuracy rates. 

_Keywords_ Obsessive-compulsive disorder · magnetic resonance imaging · major depressive disorder · schizophrenia

**Source Code** https://github.com/Tarune28/Modeling-T1-Resting-State-MRI-Variants-Using-Convolutional-Neural-Networks-in-Diagnosis-of-OCD/tree/master

**Interactive Application** https://trs-mri-application-873a0eaa5cf0.herokuapp.com/

**Contact** teswar@wpi.edu
